package com.example.countryslist;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterFlags extends RecyclerView.Adapter<AdapterFlags.ViewHolder>  {
     Activity activity;
    ArrayList<Country> list;
    List<Country> item;


 private View.OnClickListener listener;
    public AdapterFlags(ArrayList<Country> list) {
        this.list = list;
    }

    public AdapterFlags(Activity activity, ArrayList<Country> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.newrecycler,null,false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Country itemlist=list.get(position);
        holder.name.setText(list.get(position).getName());
        holder.capital.setText(list.get(position).getCapital());
        Glide.with(activity).load(list.get(position).flagUrl).into(holder.flag);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           // itemClickl.itemClick((Country) item);
                Intent intent=new Intent(holder.itemView.getContext(),Detail.class);
                intent.putExtra("itemDetail",itemlist);
                holder.itemView.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {

        return list.size();

    }





    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,capital;
        ImageView flag;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.textView);
            flag=(ImageView)itemView.findViewById(R.id.imageView);
            capital=(TextView)itemView.findViewById(R.id.textView3);
        }
    }


}
