package com.example.countryslist;

import java.io.Serializable;
import java.util.ArrayList;

public class Country implements Serializable {
    //la clase objeto, metodos get y atributos
    public String  name;
    public String hymnUrl;
    public String flagUrl;
    public String population;
    public String mapUrl;
    public String capital;
    public String continet;

    public Country() {

    }

    public Country(String name, String hymnUrl, String flagUrl, String population, String mapUrl, String capital,String continet) {
        this.name = name;
        this.hymnUrl = hymnUrl;
        this.flagUrl = flagUrl;
        this.population = population;
        this.mapUrl = mapUrl;
        this.capital = capital;
        this.continet = continet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHymnUrl() {
        return hymnUrl;
    }

    public void setHymnUrl(String hymnUrl) {
        this.hymnUrl = hymnUrl;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getContinet() {
        return continet;
    }

    public void setContinet(String continet) {
        this.continet = continet;
    }
}
