package com.example.countryslist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Detail extends AppCompatActivity {
TextView name2,capital2,population2,continent2;
ImageView map2,flag2;
WebView hy2;
private Country itemList;
private Country itemDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initViews();
        intValues();


    }

    private void initViews(){
        name2=(TextView)findViewById(R.id.nombre);
        capital2=(TextView)findViewById(R.id.capital);
        population2=(TextView)findViewById(R.id.poblacion);
        continent2=(TextView)findViewById(R.id.continente);
        map2=(ImageView)findViewById(R.id.mapa);
        hy2=(WebView)findViewById(R.id.himno);
    }
    private void intValues(){
      itemList= (Country) getIntent().getExtras().getSerializable("itemDetail");
        name2.setText("Nombre : "+ itemList.getName());
        capital2.setText("Capital : "+ itemList.getCapital());
        population2.setText("Poblacion : "+ itemList.getPopulation());
        continent2.setText("Continente : "+ itemList.getContinet());

        Glide.with(this)
                .load(itemList.getMapUrl())
                .into(map2);
        hy2.getSettings().setJavaScriptEnabled(true);
        hy2.setWebViewClient(new WebViewClient());
        hy2.loadUrl(itemList.hymnUrl);
    }

}