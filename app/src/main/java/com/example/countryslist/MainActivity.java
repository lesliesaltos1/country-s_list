package com.example.countryslist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity  {
    //=============================
    FirebaseFirestore fsdb;
    Country co;

    //=======================================
    Repository r=new Repository();
    ArrayList<Country> flagList;
    RecyclerView recyclerFv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fsdb=FirebaseFirestore.getInstance();
        flagList =new ArrayList<>();
        setContentView(R.layout.activity_main);
       recyclerFv=(RecyclerView)findViewById(R.id.myRecycler);
       recyclerFv.setLayoutManager(new LinearLayoutManager(this));
       getAll();


       AdapterFlags adapter=new AdapterFlags(this,flagList);

       recyclerFv.setAdapter(adapter);

    }

    private void getAll() {
        flagList=r.allCountries();
        //este si
        for (Country c:flagList){
            co=new Country();
            String id= UUID.randomUUID().toString();
            co.setName(c.getName());
            co.setCapital(c.getCapital());
            co.setContinet(c.getContinet());
            co.setPopulation(c.getPopulation());
            co.setFlagUrl(c.getFlagUrl());
            co.setHymnUrl(c.getHymnUrl());
            co.setMapUrl(c.getMapUrl());

            Toast.makeText(getApplicationContext(), co.getName(),Toast.LENGTH_SHORT).show();

        /*   fsdb.collection("Banderas").document(id).set(co)
                   .addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           Toast.makeText(getApplicationContext(), "Hecho",Toast.LENGTH_SHORT).show();
                       }
                   })
                   .addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception e) {
                           Toast.makeText(getApplicationContext(), "No Hecho",Toast.LENGTH_SHORT).show();
                       }
                   });*/


            fsdb.collection("Banderas")
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            List<Country> types = queryDocumentSnapshots.toObjects(Country.class);

                            flagList.addAll(types);

                            System.out.println(flagList.toString());


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });


        }


}
}