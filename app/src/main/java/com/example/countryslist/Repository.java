package com.example.countryslist;

import java.util.ArrayList;

public class Repository {

    Country c=new Country();
    public ArrayList<Country> allCountries(){
        ArrayList<Country> list = new ArrayList<>();
        list.add(new Country("Antigua y Barbuda","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/antigua-y-barbuda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Saint John's","América"));
        list.add(new Country("Argentina","https://www.youtube.com/watch?v=OqSQo2aifAA","https://www.saberespractico.com/wp-content/uploads/2017/03/argentina-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Buenos Aires","América"));
        list.add(new Country("Bahamas","https://www.youtube.com/watch?v=XONHdv9O-58","https://www.saberespractico.com/wp-content/uploads/2017/03/bahamas-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Nasáu","América"));
        list.add(new Country("Barbados","https://www.youtube.com/watch?v=KStk0fJ9G48","https://www.saberespractico.com/wp-content/uploads/2017/03/barbados-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Bridgetown","América"));
        list.add(new Country("Belice","https://www.youtube.com/watch?v=i7m0tbrgAM4","https://www.saberespractico.com/wp-content/uploads/2017/03/belice-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Belmopán","América"));
        list.add(new Country(" Bolivia","https://www.youtube.com/watch?v=rLSf8DhY2yo","https://www.saberespractico.com/wp-content/uploads/2017/03/bolivia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Sucre","América"));
        list.add(new Country("Brasil","https://www.youtube.com/watch?v=recPieyY28o","https://www.saberespractico.com/wp-content/uploads/2017/03/brasil-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Brasilia","América"));
        list.add(new Country("Canadá","https://www.youtube.com/watch?v=kUllywwhGEs","https://www.saberespractico.com/wp-content/uploads/2017/03/canada-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Ottawa","América"));
        list.add(new Country("Chile","https://www.youtube.com/watch?v=6bbKENPutWM","https://www.saberespractico.com/wp-content/uploads/2017/03/chile-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Santiago de Chile","América"));
        list.add(new Country(" Colombia","https://www.youtube.com/watch?v=RlRxaxazHbI","https://www.saberespractico.com/wp-content/uploads/2017/03/colombia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Bogotá","América"));
        list.add(new Country("Costa Rica","https://www.youtube.com/watch?v=hkPj69woq2Y","https://www.saberespractico.com/wp-content/uploads/2017/03/costa-rica-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","San José","América"));
        list.add(new Country("Cuba","https://www.youtube.com/watch?v=3Jo8nwNnHm4","https://www.saberespractico.com/wp-content/uploads/2017/03/cuba-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","La Habana","América"));
        list.add(new Country("Dominica","https://www.youtube.com/watch?v=EQtV6wz1G4E","https://www.saberespractico.com/wp-content/uploads/2017/03/dominica-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Roseau","América"));
        list.add(new Country("Ecuador","https://www.youtube.com/watch?v=LzsdNotN8jg","https://www.saberespractico.com/wp-content/uploads/2017/03/ecuador-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Quito","América"));
        list.add(new Country("El Salvador","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/el-salvador-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","San Salvador","América"));
        list.add(new Country("Estados Unidos","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/estados-unidos-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Washington D. C.","América"));
        list.add(new Country("Granada","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/granada-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Saint George","América"));
        list.add(new Country("Guatemala","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/guatemala-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Ciudad de Guatemala","América"));
        list.add(new Country("Guyana","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/guyana-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Georgetown","América"));
        list.add(new Country("Haití","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/haiti-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Puerto Príncipe","América"));
        list.add(new Country("Honduras","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/honduras-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Tegucigalpa","América"));
        list.add(new Country("Jamaica","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/jamaica-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Kingston","América"));
        list.add(new Country("México","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/mexico-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Ciudad de México","América"));
        list.add(new Country("Nicaragua","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/nicaragua-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Managua","América"));
        list.add(new Country("Panamá","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/panama-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Panamá","América"));
        list.add(new Country("Paraguay","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/paraguay-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Asunción","América"));
        list.add(new Country("Perú","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/peru-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Lima","América"));
        list.add(new Country("República Dominicana","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/antigua-y-barbuda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Santo Domingo","América"));
        list.add(new Country("San Cristóbal y Nieves","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/san-cristobal-y-nieves-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Basseterre","América"));
        list.add(new Country("San Vicente y las Granadinas","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/san-vincent-y-las-granadinas-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Kingstown","América"));
        list.add(new Country("Santa Lucía","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/santa-lucia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Castries","América"));
        list.add(new Country("Surinam","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/antigua-y-barbuda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Paramaribo","América"));
        list.add(new Country("Trinidad y Tobago","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/uruguay-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Puerto España","América"));
        list.add(new Country("Uruguay","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/antigua-y-barbuda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Montevideo","América"));
        list.add(new Country("Venezuela","https://www.youtube.com/watch?v=-n8amVqdTlg","https://www.saberespractico.com/wp-content/uploads/2017/03/venezuela-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/america-capitales-150x150.jpg","Caracas","América"));



        list.add(new Country("Alemania","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/alemania-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Berlín","Europa"));
        list.add(new Country("Austria","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/austria-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Andorra La Vieja","Europa"));
        list.add(new Country("Bélgica","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/belgica-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Ereván","Europa"));
        list.add(new Country("Chipre ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/chipre-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Bakú","Europa"));
        list.add(new Country("Croacia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/croacia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Bruselas","Europa"));
        list.add(new Country("Dinamarca","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/dinamarca-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Minsk","Europa"));
        list.add(new Country("Eslovaquia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/eslovaquia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Sarajevo","Europa"));
        list.add(new Country("Eslovenia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/eslovenia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Sofía","Europa"));
        list.add(new Country("España","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/espana-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Madrid","Europa"));

        list.add(new Country("Estonia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/estonia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Tallín","Europa"));
        list.add(new Country("Finlandia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/finlandia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Helsinki","Europa"));
        list.add(new Country("Francia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/francia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","París","Europa"));
        list.add(new Country("Grecia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/grecia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Tiflis","Europa"));
        list.add(new Country("Hungría","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/hungria-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Berlín","Europa"));
        list.add(new Country("Irlanda","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/italia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Atenas","Europa"));
        list.add(new Country("Italia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/irlanda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Dublín","Europa"));
        list.add(new Country("Letonia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/letonia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Reikiavik","Europa"));
        list.add(new Country("Lituania","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/lituania-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Vilna","Europa"));
        list.add(new Country("Luxemburgo","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/luxemburgo-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Luxemburgo","Europa"));
        list.add(new Country("Malta","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/malta-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Skopie","Europa"));
        list.add(new Country("Países ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/malta-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","La Valeta","Europa"));
        list.add(new Country("Polonia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/polonia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Chisinau","Europa"));

        list.add(new Country("Portugal","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/portugal-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Lisboa","Europa"));
        list.add(new Country("República Checa","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/republica-checa-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Praga","Europa"));
        list.add(new Country("Rumanía","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/rumania-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Bucarest","Europa"));
        list.add(new Country("Suecia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/suecia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2017/04/bandera-union-europea-125x125.jpg","Estocolmo","Europa"));


        //=============

        list.add(new Country("Afganistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/afganistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Kabul","Asia"));
        list.add(new Country("Arabia Saudita","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/arabia-saudita-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Riad","Asia"));
        list.add(new Country("Armenia ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/armenia-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Ereván","Asia"));
        list.add(new Country("Azerbaiyán ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/azerbaiyan-bandera-25px-2.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Bakú","Asia"));
        list.add(new Country("Bangladés","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/banglades-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Manama","Asia"));
        list.add(new Country("Baréin","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/barein-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Naipyidó","Asia"));
        list.add(new Country("Birmania / Myanmar","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/birmania-myanmar-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Bandar Seri Begawan","Asia"));
        list.add(new Country("Brunéi","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/brunei-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Timbu","Asia"));
        list.add(new Country("Bután","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/butan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Nom Pen","Asia"));
        list.add(new Country("Camboya","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/camboya-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Nom Pen","Asia"));
        list.add(new Country("China","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/china-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Pekín","Asia"));
        list.add(new Country("Corea del Norte","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/corea-del-norte-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Pionyang","Asia"));
        list.add(new Country("Corea del Sur","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/corea-del-sur-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Seúl","Asia"));
        list.add(new Country("Emiratos Árabes Unidos","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/emiratos-arabes-unidos-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Abu Dabi","Asia"));
        list.add(new Country("Filipinas","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/filipinas-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Manila","Asia"));
        list.add(new Country("Georgia ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/georgia-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Tiflis","Asia"));
        list.add(new Country("India","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/india-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Nueva Delhi","Asia"));
        list.add(new Country("Indonesia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/indonesia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Yakarta","Asia"));
        list.add(new Country("Irak","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/irak-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Bagdad","Asia"));
        list.add(new Country("Irán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/iran-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Teherán","Asia"));
        list.add(new Country("Israel","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/israel-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Jerusalén","Asia"));
        list.add(new Country("Japón","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/japon-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Tokio","Asia"));
        list.add(new Country("Jordania","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/jordania-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Amán","Asia"));
        list.add(new Country("Kazajistán ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/kazajistan-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Astaná","Asia"));
        list.add(new Country("Kirguistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/kirguistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Biskek","Asia"));
        list.add(new Country("Kuwait","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/kuwait-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Kuwait","Asia"));
        list.add(new Country("Laos","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/laos-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Vientián","Asia"));
        list.add(new Country("Líbano","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/libano-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Beirut","Asia"));
        list.add(new Country("Malasia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/malasia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Kuala Lumpur","Asia"));
        list.add(new Country("Maldivas","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/maldivas-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Malé","Asia"));
        list.add(new Country("Mongolia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/mongolia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Ulán Bator","Asia"));
        list.add(new Country("Nepal","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/nepal-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Katmandú","Asia"));
        list.add(new Country("Omán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/oman-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Mascate","Asia"));
        list.add(new Country("Pakistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/pakistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Islamabad","Asia"));
        list.add(new Country("Rusia  ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/rusia-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Moscú","Asia"));
        list.add(new Country("Singapur","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/singapur-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Singapur","Asia"));
        list.add(new Country("Siria","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/siria-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Damasco","Asia"));
        list.add(new Country("Sri Lanka","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/sri-lanka-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Sri Jayawardenapura Kotte","Asia"));
        list.add(new Country("Tayikistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/tayikistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Dusambé","Asia"));
        list.add(new Country("Tailandia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/tailandia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Bangkok","Asia"));
        list.add(new Country("Timor Oriental","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/timor-oriental-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Dili","Asia"));
        list.add(new Country("Turkmenistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/turkmenistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Asjabad","Asia"));
        list.add(new Country("Turquía ","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/turquia-bandera-25px-1.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Ankara","Asia"));
        list.add(new Country("Uzbekistán","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/uzbekistan-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Taskent","Asia"));
        list.add(new Country("Vietnam","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/vietnam-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Hanói","Asia"));
        list.add(new Country("Yemen","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2017/03/yemen-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/uploads/2011/07/mapa-asia-capitales-150x150.jpg","Saná","Asia"));



        list.add(new Country("Australia","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/australia-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Camberra","Oceania"));


        list.add(new Country("Fiyi","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/fiyi-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Suva","Oceania"));

        list.add(new Country("Islas Marshall","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/islas-marshall-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Majuro","Oceania"));
        list.add(new Country("Islas Salomón","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/islas-salomon-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Tarawa","Oceania"));
        list.add(new Country("Nauru","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/nauru-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Palikir","Oceania"));
        list.add(new Country("Nueva Zelanda","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/nueva-zelanda-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Yaren","Oceania"));
        list.add(new Country("Palaos","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/palaos-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Wellington","Oceania"));
        list.add(new Country("Papúa Nueva Guinea","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/papua-nueva-guinea-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Melekeok o Ngerulmud","Oceania"));
        list.add(new Country("Samoa","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/samoa-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Port Moresby","Oceania"));
        list.add(new Country("Tonga","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/tonga-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Apia","Oceania"));
        list.add(new Country("Tuvalu","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/tuvalu-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Nukualofa","Oceania"));

        list.add(new Country("Vanuatu","https://www.youtube.com/watch?v=YYrXbV6hp7g",
                "https://www.saberespractico.com/wp-content/uploads/2011/07/vanuatu-bandera-25px.jpg","5.430.25"
                ,"https://www.saberespractico.com/wp-content/themes/imagination/Pa%C3%ADses%20de%20Ocean%C3%ADa%20(entrada).jpg","Port Vila","Oceania"));

        return list;
    }
}

